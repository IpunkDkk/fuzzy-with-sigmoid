import math
import statistics

from scipy.integrate import quad, cumulative_trapezoid

def alfaPredikat(a, b, data1, data2,data3):
    # rumus dibawah merupakan aturan fuzzy
    data = {'bawah': [], 'atas':[], 't':[]}
    nilaiAwal = min(data3)
    nilaiAkhir = max(data3)
    alfa1= min(kurvaPenyusutan(a, data1), kurvaPenyusutan(b, data2))
    alfa2= min(kurvaPenyusutan(a, data1), kurvaPertumbuhan(b, data2))
    alfa3= min(kurvaPertumbuhan(a, data1), kurvaPenyusutan(b, data2))
    alfa4= min(kurvaPertumbuhan(a, data1), kurvaPertumbuhan(b, data2))
    data['bawah'].append(max(alfa1,alfa2))
    data['t'].append(round((math.sqrt((data['bawah'][0]*math.pow(nilaiAkhir-nilaiAwal,2))/2 ))+nilaiAwal,2))
    data['atas'].append(max(alfa3,alfa4))
    data['t'].append(round((math.sqrt((data['atas'][0]*math.pow(nilaiAkhir-nilaiAwal,2))/2 ))+nilaiAwal,2))
    return data


def kurvaPertumbuhan(x, data):
    nilaiAwal = min(data)
    nilaiAkhir = max(data)
    nilaiTengah = statistics.mean(data)
    if x <= nilaiAwal:
        hasil = 0
    elif nilaiAwal <= x <= nilaiTengah:
        hasil = 2 * (math.pow((x - nilaiAwal) / (nilaiAkhir - nilaiAwal), 2))
    elif nilaiTengah <= x <= nilaiAkhir:
        hasil = 1 - 2 * (math.pow((nilaiAkhir - x) / (nilaiAkhir - nilaiAwal), 2))
    else:
        hasil = 1
    hasil = round(hasil, 2)
    return hasil


def kurvaPenyusutan(x, data):
    nilaiAwal = min(data)
    nilaiAkhir = max(data)
    nilaiTengah = statistics.mean(data)
    if x <= nilaiAwal:
        hasil = 1
    elif nilaiAwal <= x <= nilaiTengah:
        hasil = 1 - 2 * (math.pow((x - nilaiAwal) / (nilaiAkhir - nilaiAwal), 2))
    elif nilaiTengah <= x <= nilaiAkhir:
        hasil = 2 * (math.pow(((nilaiAkhir - x) / (nilaiAkhir - nilaiAwal)), 2))
    else:
        hasil = 0
    hasil = round(hasil, 2)
    return hasil

def defusifikasi(data, produksi):
    nilaiAwal = min(produksi)
    nilaiAkhir = max(produksi)
    nilait1 = data['t'][0]
    nilait2 = data['t'][1]
    tmp = {'moment':[], 'luas':[]}
    # fungsi baru
    print("fungsi baru")
    print("jika z lebih kecil dari {} maka z adalah {}" .format(data['t'][0], data['bawah'][0]))
    print("jika z diantara {} dengan {} maka z adalah 2((z-{})/({}-{}))^2".format(data['t'][0], data['t'][1], nilaiAwal, nilaiAkhir, nilaiAwal))
    print("jika z lebih besar dari {} maka z adalah {}".format(data['t'][1], data['atas'][0]))

    def mfungsi1(x):
        return data['bawah'][0] * x
    def mfungsi2(x):
        rumus = (2*( math.pow((x-nilaiAwal)/(nilaiAkhir-nilaiAwal),2)))*x
        return rumus
    def mfungsi3(x):
        return data['atas'][0] * x

    def lfungsi1(x):
        return data['bawah'][0]
    def lfungsi2(x):
        rumus = (2*( math.pow((x-nilaiAwal)/(nilaiAkhir-nilaiAwal),2)))
        return rumus
    def lfungsi3(x):
        return data['atas'][0]
    tmp['moment'].append(round(quad(mfungsi1, nilaiAwal, nilait1)[0],2))
    tmp['moment'].append(round(quad(mfungsi2, nilait1, nilait2)[0],2))
    tmp['moment'].append(round(quad(mfungsi3, nilait2, nilaiAkhir)[0],2))
    tmp['luas'].append(round(quad(lfungsi1, nilaiAwal, nilait1)[0], 2))
    tmp['luas'].append(round(quad(lfungsi2, nilait1, nilait2)[0], 2))
    tmp['luas'].append(round(quad(lfungsi3, nilait2, nilaiAkhir)[0], 2))
    return tmp
def hitung(defusifikasi):
    mtmp = 0
    atmp = 0
    for m,a in zip(defusifikasi['moment'], defusifikasi['luas']):
        mtmp += m
        atmp += a
    hasil = round(mtmp/atmp,2)
    return hasil




permintaan = [201, 108, 111, 112, 211, 212, 221, 203, 189, 197, 167, 154, 136, 204, 208, 109, 204, 207, 245, 256]
persediaan = [56, 45, 34, 64, 23, 19, 16, 39, 29, 35, 62, 55, 34, 55, 34, 23, 56, 54, 43, 44]
produksi = [444, 455, 456, 345, 234, 433, 234, 432, 334, 345, 432, 324, 434, 456, 444, 345, 345, 443, 442, 324]

ppermintaan = 197
ppersediaan = 34
predikat = alfaPredikat(ppermintaan, ppersediaan, permintaan, persediaan, produksi)
print(predikat)
hasil = hitung(defusifikasi(predikat, produksi))
print("mamdani")
print("jika permintaan sebanyak {} dan persediaan {} maka produksi adalah {} ". format(ppermintaan, ppersediaan,hasil))


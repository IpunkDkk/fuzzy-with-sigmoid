import math
import statistics


def alfaPredikat(a, b, data1, data2, data3):
    nilaiAwal = min(data3)
    nilaiAkhir = max(data3)
    data = {'predikat': [], 'z': []}
    data['predikat'].append(min(kurvaPenyusutan(a, data1), kurvaPenyusutan(b, data2)))
    data['z'].append(
        round(-1 * (math.sqrt(((data['predikat'][0] * (math.pow(nilaiAkhir - nilaiAwal, 2))) / 2)) - nilaiAkhir), 2))
    data['predikat'].append(min(kurvaPenyusutan(a, data1), kurvaPertumbuhan(b, data2)))
    data['z'].append(
        round(-1 * (math.sqrt(((data['predikat'][1] * (math.pow(nilaiAkhir - nilaiAwal, 2))) / 2)) - nilaiAkhir), 2))
    data['predikat'].append(min(kurvaPertumbuhan(a, data1), kurvaPenyusutan(b, data2)))
    data['z'].append(
        round((-1 * (math.sqrt(((data['predikat'][2] * (math.pow(nilaiAkhir - nilaiAwal, 2))) / 1)) - nilaiAkhir))+2, 2))
    data['predikat'].append(min(kurvaPertumbuhan(a, data1), kurvaPertumbuhan(b, data2)))
    data['z'].append(
        round((-1 * (math.sqrt(((data['predikat'][3] * (math.pow(nilaiAkhir - nilaiAwal, 2))) / 1)) - nilaiAkhir))+2, 2))
    return data


def kurvaPertumbuhan(x, data):
    nilaiAwal = min(data)
    nilaiAkhir = max(data)
    nilaiTengah = statistics.mean(data)
    if x <= nilaiAwal:
        hasil = 0
    elif nilaiAwal <= x <= nilaiTengah:
        hasil = 2 * (math.pow((x - nilaiAwal) / (nilaiAkhir - nilaiAwal), 2))
    elif nilaiTengah <= x <= nilaiAkhir:
        hasil = 1 - 2 * (math.pow((nilaiAkhir - x) / (nilaiAkhir - nilaiAwal), 2))
    else:
        hasil = 1
    hasil = round(hasil, 2)
    return hasil


def kurvaPenyusutan(x, data):
    nilaiAwal = min(data)
    nilaiAkhir = max(data)
    nilaiTengah = statistics.mean(data)
    if x <= nilaiAwal:
        hasil = 1
    elif nilaiAwal <= x <= nilaiTengah:
        hasil = 1 - 2 * (math.pow((x - nilaiAwal) / (nilaiAkhir - nilaiAwal), 2))
    elif nilaiTengah <= x <= nilaiAkhir:
        hasil = 2 * (math.pow(((nilaiAkhir - x) / (nilaiAkhir - nilaiAwal)), 2))
    else:
        hasil = 0
    hasil = round(hasil, 2)
    return hasil

def hitung(data):
    tmpAll = 0
    tmpPred = 0
    for predikat, z in zip(data['predikat'],data['z']):
        tmpAll += predikat*z
        tmpPred += predikat
    hasil = round(tmpAll / tmpPred,2)
    return hasil
permintaan = [201, 108, 111, 112, 211, 212, 221, 203, 189, 197, 167, 154, 136, 204, 208, 109, 204, 207, 245, 256]
persediaan = [56, 45, 34, 64, 23, 19, 16, 39, 29, 35, 62, 55, 34, 55, 34, 23, 56, 54, 43, 44]
produksi = [444, 455, 456, 345, 234, 433, 234, 432, 334, 345, 432, 324, 434, 456, 444, 345, 345, 443, 442, 324]

ppermintaan = 197
ppersediaan = 34
predikat = alfaPredikat(197, 34, permintaan, persediaan, produksi)
print(predikat)
hasil = hitung(predikat)
print("tsukamoto")
print("jika permintaan sebanyak {} dan persediaan {} maka produksi adalah {} ". format(ppermintaan, ppersediaan,hasil))

